<?php
include "config.php";

$koneksi = new Database();

$koneksi->tambah_data_peminjam(
    $_POST['kode_peminjam'],
    $_POST['nama_peminjam'],
    $_POST['jenis_kelamin'],
    $_POST['tanggal_lahir'],
    $_POST['alamat'],
    $_POST['pekerjaan']
);

// Add a missing semicolon at the end of the header line
header("location: tampil_p.php");
?>
