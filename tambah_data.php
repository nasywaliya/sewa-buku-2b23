<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <?php
    include "config.php";
    $db = new Database();
  ?>

  <h3>Tasboh Data Peminjam</h3>

  <form action="simpan_data_peminjam.php" method="post">
    <table>
      <tr>
        <td>Kode peminjan</td>
        <td><input type="text" name="kode_peminjan"></td>
      </tr>
      <tr>
        <td>Nama</td>
        <td><input type="text" name="nama_peminjan"></td>
      </tr>
      <tr>
        <td>Jenis Kelamin</td>
        <td>
          <select name="jenis_kelamin">
            <?php
              $i = 1;
              foreach($db->tampil_data_jenis_kelamin() as $x) {
                echo "<option value='".$x['kode_Jk']."'>".$x['keterangan_jk']."</option>";
                $i++;
              }
            ?>
          </select>
        </td>
      </tr>
    </table>
    <input type="submit" value="Submit">
  </form>
</body>
</html>
